﻿using System.Web.Mvc;

namespace vue_mvc.Controllers
{
    public class HomeController : Controller
    {
        // GET: VueRouter
        public ActionResult Index()
        {
            return View();
        }
    }
}